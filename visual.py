from utils import plyfile
import numpy as np
import random
import os

def randomcolor():
    def getOne():
        color = random.randint(1, 16)
        color *= 16
        color += random.randint(1, 16)
        return color
    return getOne(), getOne(), getOne()

pred_data_label_filenames = []
file_name = 'models/ASIS/log5_test/output_filelist.txt'
pred_data_label_filenames += ['models/ASIS/'+line.rstrip() for line in open(file_name)]


output_dir = 'outputply'



def asigncolor(pro_data):
    # data: (N,8) xyzrgbis
    ins = pro_data[:, -2].astype(np.int)
    sem = pro_data[:, -1].astype(np.int)
    for ins_idx in np.unique(ins):
        if ins_idx == -1:
            pro_data[ins==-1, 3] = 255
            pro_data[ins==-1, 4] = 255
            pro_data[ins==-1, 5] = 255
            continue
        cur_class = np.argmax(np.bincount(sem[ins==ins_idx]))
        if cur_class in (0,1,2,5,6):
            r, g, b = randomcolor()
            pro_data[ins==ins_idx, 3] = r
            pro_data[ins==ins_idx, 4] = g
            pro_data[ins==ins_idx, 5] = b
        else:
            pro_data[ins==ins_idx, 3] = 255
            pro_data[ins==ins_idx, 4] = 255
            pro_data[ins==ins_idx, 5] = 255
    return pro_data

def save2ply(save_data, filename='my.ply'):
    # data (N, 6)  xyzrgb
    print save_data.shape
    vertex = np.array([tuple(i) for i in save_data],
                      dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4'), ('red', 'u1'), ('green', 'u1'), ('blue', 'u1')])
    el = plyfile.PlyElement.describe(vertex, 'vertex')
    plyfile.PlyData([el]).write(filename)

def createply(pred_root, gt_root, outname):
    # gt_root = 'models/ASIS/log5_test/test_results/Area_5_conferenceRoom_2_gt.txt'
    # pred_root = 'models/ASIS/log5_test/test_results/Area_5_conferenceRoom_2_pred.txt'
    data_label = np.loadtxt(pred_root)
    pred_ins = data_label[:, -1].reshape(-1).astype(np.int)
    pred_sem = data_label[:, -2].reshape(-1).astype(np.int)
    gt_label = np.loadtxt(gt_root)
    gt_ins = gt_label[:, -1].reshape(-1).astype(np.int)
    gt_sem = gt_label[:, -2].reshape(-1).astype(np.int)
    data = data_label[:, :8]
    data[:, -2] = pred_ins[:]
    data[:, -1] = pred_sem[:]


    data = asigncolor(data)
    save2ply(data[:, :6], outname+'_origin.ply')
    data[:, -2] = gt_ins[:]
    data[:, -1] = gt_sem[:]
    data = asigncolor(data)
    save2ply(data[:, :6], outname+'_pred.ply')

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

for f in pred_data_label_filenames:
    outfile = f.rstrip('_pred\.txt').split('/')[-1]
    createply(f, f.rstrip('_pred\.txt') + '_gt.txt', output_dir + '/' + outfile)



