from utils import plyfile
import numpy as np
import random
import os

pred_data_label_filenames = []
file_name = 'models/ASIS/log5_test/output_filelist.txt'
pred_data_label_filenames += ['models/ASIS/'+line.rstrip() for line in open(file_name)]

output_dir = 'outputtxt'

def createtxt(pred_root, gt_root, outname):
    # gt_root = 'models/ASIS/log5_test/test_results/Area_5_conferenceRoom_2_gt.txt'
    # pred_root = 'models/ASIS/log5_test/test_results/Area_5_conferenceRoom_2_pred.txt'
    data_label = np.loadtxt(pred_root)
    gt_label = np.loadtxt(gt_root)
    gt_ins = gt_label[:, -1].reshape(-1).astype(np.int)
    gt_sem = gt_label[:, -2].reshape(-1).astype(np.int)
    data = data_label[:, :3]

    fout = open(outname, 'w')
    for i in range(data.shape[0]):
        fout.write('%f %f %f %d %d\n' % \
                   (data_label[i, 0], data_label[i, 1], data_label[i, 2],
                    gt_sem[i], gt_ins[i]))
    fout.close()


if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

for f in pred_data_label_filenames:
    outfile = f.rstrip('_pred\.txt').split('/')[-1] + '.txt'
    createtxt(f, f.rstrip('_pred\.txt') + '_gt.txt', output_dir + '/' +outfile)



