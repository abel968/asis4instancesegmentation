# 使用ASIS将房间内的建筑类的实例分割出来
## 使用python2.7
1. 安装anaconda (随便你，直接安装本机环境也行)
2. 安装cuda
3. 安装tensorflow   

   cuda版本尽量和tensorflow对应吧
   我是cuda10.0 tf1.14.0 (conda install tensorflow-gpu=1.14)
4. 安装h5py,sklearn, cv2, IPython, matplotlib

   pip install -i https://pypi.tuna.tsinghua.edu.cn/simple h5py sklearn opencv-python IPython matplotlib
5. cd tf_ops进行编译

    注意： grouping和sampling都需要进文件修改nvcc路径
    - cd 3d_interpolation
    
        sh tf_interpolate_compile.sh
        
        测试： python tf_interpolate_op_test.py
    - cd grouping
    
        sh tf_grouping_compile.sh
        
        测试： python tf_grouping_op_test.py
    - cd sampling
    
        sh tf_sampling_compile.sh
        
        测试： python tf_sampling.py
6. 将数据Stanford3dDataset_v1.2_Aligned_Version放在data目录下
7. python collect_indoor3d_data.py
    这步主要是将原来的txt文件转成npy文件
    一个npy文件表示一个Area中的一个房间 如Area_1_hallway_7
    数据格式为xyzrgbsi  s为语义标签 i为实例标签(每个房间的实例都是从标号0开始)
    xyz为原始坐标减去min(xyz)
8. python gen_h5.py
    将每个房间生成多个block  每个block采样点4096
    一个房间npy文件  对应一个blocks h5文件
9. cd data && python generate_input_list.py
10. 将三个epoch_99开头的模型文件放在ASIS/model/ASIS/log5目录下
11. cd models/ASIS/

    ln -s ../../data .
12. cd models/ASIS/
    python test.py --gpu 0 --bandwidth 0.6 --log_dir log5_test --model_path log5/epoch_99.ckpt --input_list  meta/area5_data_label.txt --verbose
13. 回到最开始的ASIS目录
    python generate.py
    
    在当前目录生成outputtxt目录，里面是一堆txt文件。
    每个txt文件表示Area5中的一个房间。每一行代表一个点的xyzsi.
    s表示预测的该点类别id  i表示预测的该点实例id
14. （可选）可视化
    python visual.py
    
    在当前目录生成outputply目录，里面是一堆ply文件。
    每个Area5的房间对应两个ply文件 origin为原始的 pred为预测的。
    天花板 地板 墙 床 门 各个实例随机赋色。其余类型的实例赋白色。
